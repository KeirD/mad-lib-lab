
// Mad Libs
// Keir Dvorachek

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void Print(string* collection, ostream &out)
{
	out << "One day my " << collection[0] << " friend and I decided to go to the " << collection[1] << " game in " << collection[2] << ". \n";
	out << "We really wanted to see " << collection[3] << " play.\n";
	out << "So we " << collection[4] << " in the " << collection[5] << " and headed down to the " << collection[6] << " and bout some " << collection[7] << ".\n";
	out << "We watched the game and it was " << collection[8] << ".\n";
	out << "We at some " << collection[9] << " and drank some " << collection[10] << ".\n";
	out << "We had a " << collection[11] << "time, and can't wait to go again. \n";
}

int main()
{
	const int NUM_WORDS = 12;
	string arrCollection[NUM_WORDS];
	string words[NUM_WORDS] = { "Enter an adjective <describing word>: ", "Enter a sport: ", "Enter a city name: ", "Enter a person: ",
		"Enter and action verb <past tense>: ", "Enter a vehicle: ", "Enter a place: ", "Enter a noun <thing, plural>: ", 
		"Enter an adjective <describing word>: ", "Enter a food <plural>: ", "Enter a liquid: ", "Enter and adjective <describing word>: " };

	for (int i = 0; i < 12; i++)
	{
		cout << words[i];
		getline(cin, arrCollection[i]);
	}

	Print(arrCollection, cout);

	string filepath = "C:\\temp\\MadLib.txt";

	cout << "Would you like to save the file?(y/n)";
	char input;
	cin >> input;
	if (input == 'y' || input == 'Y')
	{
		ofstream fout(filepath);
		Print(arrCollection, fout);
		fout.close;
	}

	_getch();
	return 0;
}
